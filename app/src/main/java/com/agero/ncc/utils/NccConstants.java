package com.agero.ncc.utils;



public class NccConstants {

    public static final String SOURCE = "FIREBASE";
    public static final String SUB_SOURCE = "SPARK_ANDROID";
    public static final String APIGEE_TOKEN = "id_token";
    public static final String APIGEE_TOKEN_EXPIRE = "expires_in";
    public static final String FIREBASE_TOKEN = "firebase_token";
    public static final String FIREBASE_REFRESH_TOKEN = "refresh_token";
    public static final String IS_REFRESH_TOKEN_FAILED = "isFailed";
    public static final String DEVICE_ID = "deviceId";

    public static final String BOTTOMBARSELECTED = "BottomBarSelected";
    public static final String EQUIPMENT_PREF_KEY = "Equipmentprefkey";
    public static final String EQUIPMENT_POSITION = "Equipmentpos";
    public static final String EQUIPMENT_ID = "Equipmentid";
    public static final String KEY_FROM_SCREEN = "FromScreen";

    public static final String JOB_STATUS_OFFERED = "OFFERED";
    public static final String JOB_STATUS_REQUESTED = "REQUESTED";
    public static final String JOB_STATUS_ACCEPTED = "ACCEPTED";
    public static final String JOB_STATUS_DECLINED = "DECLINED";
    public static final String JOB_STATUS_REJECTED = "REJECTED";
    public static final String JOB_STATUS_EXPIRED = "EXPIRED";
    public static final String JOB_STATUS_AWARDED = "AWARDED";
    public static final String JOB_STATUS_UNASSIGNED = "UNASSIGNED";
    public static final String JOB_STATUS_ASSIGNED = "ASSIGNED";
    public static final String JOB_STATUS_EN_ROUTE = "ENROUTE";
    public static final String JOB_STATUS_ON_SCENE = "ONSCENE";
    public static final String JOB_STATUS_TOWING = "TOW_PROGRESS";
    public static final String JOB_STATUS_DESTINATION_ARRIVAL = "TOW_DESTINATION_ARRIVED";
    public static final String JOB_STATUS_PLACE_HOLD = "TOW_TO_BE_COMPLETED";
    public static final String JOB_STATUS_JOB_COMPLETED = "JOB_COMPLETED";
    public static final String JOB_STATUS_GOA = "GOA";
    public static final String JOB_STATUS_UNSUCCESSFUL = "UNSUCCESSFUL";
    public static final String JOB_STATUS_DENIED = "DENIED";
    public static final String JOB_STATUS_PAUSED = "PAUSED";
    public static final String JOB_STATUS_CANCELLED = "CANCELLED";

    public static final String SIGNIN_EMAILADDRESS = "Signin_EmailAddress";
    public static final String SIGNIN_PASSWORD = "Signin_Password";
    public static final String SIGNIN_PASSWORD_IV = "Signin_Password_Iv";
    public static final String SIGNIN_VENDOR_ID = "VendorId";
    public static final String SIGNIN_USER_ID = "UserId";
    public static final String SIGNIN_TOUSER_ID = "UserToId";
    public static final String SIGNIN_USER_NAME = "UserName";
    public static final String SIGNIN_TOUSER_NAME = "UserToName";
    public static final String SIGNIN_USER_ROLE = "UserRole";

    public static final String USER_ROLE_DRIVER = "ncc_driver";
    public static final String USER_ROLE_DISPATCHER = "ncc_dispatch_manager";
    public static final String USER_ROLE_TEAM_MANAGER = "ncc_driver_equipment_manager";
    public static final String USER_ROLE_AGERO_NETWORK_ADMIN = "ncc_agero_network_manager";
    public static final String USER_ROLE_PARTNER_ADMIN = "ncc_partner_administrator";
    public static final String USER_ROLE_BILLING_MANAGER = "ncc_billing_manager";

    public static final String CAMERA_IMAGE_PREF_KEY = "Image_Pref_Key";
    public static final String PREF_KEY_REGISTRATION_TOKEN = "PREF_KEY_REGISTRATION_TOKEN";

    public static final String INTENT_NOTIFICATION_KEY_DRIVER_ID = "DriverId";
    public static final String INTENT_NOTIFICATION_KEY_VENDOR_ID = "VendorId";

    public static final String PREF_KEY_DIRECTION_RESPONSE = "DirectionResponse";

    public static final String PREF_KEY_NOTIFICATION_URI = "NotificationUri";

    public static final String BUNDLE_KEY_ADD_NEW = "AddNew";
    public static final String BUNDLE_KEY_IS_BOTTOM_VISIBLE = "isBottomVisible";
    public static final String SHOW_ON_OFF_DUTY = "showOnOffDuty";
    public static final String IS_FROM_CHAT = "isFromChat";
    public static final String JOB_ID = "jobId";
    public static final String IS_JOB_COMPLETED = "isJobCompleted";


    public static class FirebaseLogKey {
        public static final String KEY_BOTTOM_BAR = "ncc_bottom_bar";
        public static final String KEY_BOTTOM_BAR_JOBS = "ncc_bottom_bar_jobs";
        public static final String KEY_BOTTOM_BAR_HISTORY = "ncc_bottom_bar_history";
        public static final String KEY_BOTTOM_BAR_ALERTS = "ncc_bottom_bar_alerts";
        public static final String KEY_BOTTOM_BAR_CHAT = "ncc_bottom_bar_chat";
        public static final String KEY_BOTTOM_BAR_ACCOUNT = "ncc_bottom_bar_account";
    }

    public static  class FirebaseEventAction {
        public static  final  String CREATE_ACCOUNT = "CreateAccount";
        public static  final  String SIGN_IN = "SignIn";
        public static  final  String CONTINUE = "Continue";
        public static  final  String AGREE = "Agree";
        public static  final  String ALLOW_PERMISSION = "AllowPermission";
        public static  final  String SKIP_PERMISSION = "SkipPermission";
        public static  final  String FORGOT_PASSWORD = "ForgotPassword";
        public static  final  String RESET_PASSWORD = "ResetPassword";
        public static  final  String VIEW_NOTES_TAG = "ViewNotesTag";
        public static  final  String VIEW_MAP = "ViewMap";
        public static  final  String VIEW_NOTES = "ViewNotes";
        public static  final  String MODIFY_STATUS = "ModifyStatus";
        public static  final  String UPDATE_ETA = "UpdateETA";
        public static  final  String ASSIGN_DRIVER = "AssignDriver";
        public static  final  String GET_DIRECTIONS = "GetDirections";
        public static  final  String CALL_TOW_DESTINATION = "CallTowDestination";
        public static  final  String CALL_CUSTOMER ="CallCustomer";
        public static  final  String START_JOB = "StartJob";
        public static  final  String ADD_ATTACHMENTS = "AddAttachments";
        public static  final  String ADD_DISABLEMENT_SIGNATURE = "AddDisablementSignature";
        public static  final  String ADD_TOW_DEST_SIGNATURE = "AddTowDestSignature";
        public static  final  String VIEW_ATTACHMENTS = "ViewAttachments";
        public static  final  String VIEW_DISABLEMENT_SIGNATURE = "ViewDisablementSignature";
        public static  final  String VIEW_TOW_DEST_SIGNATURE = "ViewTowDestSignature";
        public static  final  String AS_MODIFY_STATUS = "ASModifyStatus";
        public static  final  String AS_UPDATE_ETA = "ASUpdateETA";
        public static  final  String AS_ASSIGN_DRIVER = "ASAssignDriver";
        public static  final  String AS_EDIT_SERVICE = "ASEditService";
        public static  final  String AS_REPORT_GOA ="ASReportGOA";
        public static  final  String AS_SERVICE_UNSUCCESSFUL = "ASServiceUnsucessful";
        public static  final  String AS_CANCEL_SERVICE = "ASCancelService";
        public static  final  String AS_CHANGE_DISABLEMENT = "ASChangeDisablement";
        public static  final  String AS_CHANGE_TOW_DESTINATION = "ASChangeTowDestination";
        public static  final  String AS_CALL_CUSTOMER = "ASCallCustomer";
        public static  final  String AS_CALL_DISPATCHER = "ASCallDispatcher";
        public static  final  String AS_CALL_AGERO = "ASCallAgero";
        public static  final  String DELETE_ALL = "DeleteAll";
        public static  final  String VIEW_ALERT_DETAIL = "ViewAlertDetail";
        public static  final  String DELETE_ONE = "DeleteOne";
        public static  final  String VIEW_ACCOUNT = "ViewAccount";
        public static  final  String CHAT_LIST = "ChatList";
        public static  final  String VIEW_HISTORY = "ViewHistory";
        public static  final  String VIEW_JOBS = "ViewJobs";
        public static  final  String VIEW_PROFILE = "ViewProfile";
        public static  final  String VIEW_COMPANY = "ViewCompany";
        public static  final  String CHANGE_SOUND = "Change_Sound";
        public static  final  String VIEW_HELP = "ViewHelp";
        public static  final  String VIEW_LEGAL = "View_Legal";
        public static  final  String SIGN_OUT = "SignOut";
        public static  final  String VIEW_ALERTS = "ViewAlerts";
        public static  final  String VIEW_HISTORY_DETAIL = "ViewHistoryDetail";
        public static  final  String SEARCH_DATE = "SearchDate";
        public static  final  String SEARCH_JOB_ID = "SearchJobID";
        public static  final  String GO_OFF_DUTY = "GoOffDuty";
        public static  final  String CHANGE_EQUIPMENT = "ChangeEquipment";
        public static  final  String GO_ON_DUTY = "GoOnDuty";
        public static  final  String VIEW_JOB_DETAIL = "ViewJobDetail";
        public static  final  String VIEW_TIME_LINE = "ViewTimeline";
    }

    public static  class  FirebaseEventCategory {
        public static  final  String LANDING  = "Landing";
        public static  final  String CREATE_ACCOUNT = "CreateAccount";
        public static  final  String LEGAL = "Legal";
        public static  final  String SIGN_IN = "SignIn";
        public static  final  String PERMISSION_REQUEST = "PermissionRequest";
        public static  final  String JOB_DETAILS = "JobDetails";
        public static  final  String ALERTS = "Alerts";
        public static  final  String JOB_HISTORY_DETAILS = "JobHistoryDetail";
        public static  final  String HISTORY = "History";
        public static  final  String ACCOUNT = "Account";
        public static  final  String JOBS = "Jobs";
        public static  final  String CHAT = "Chat";
        public static  final  String HISTORY_SEARCH = "HistorySearch";
    }

    public static class BackgroundCheckStatus{
        public static  final  String SUBMITTED = "SU";//Submitted
        public static  final  String NOT_SUBMITTED = "NS";//NotSubmitted
        public static  final  String VERIFIED = "VD";//Verified
        public static  final  String NOT_VERIFIED = "NV";//NotVerified
        public static  final  String UNDER_LEGAL_REVIEW = "UR";//UnderlegalReview
        public static  final  String VERIFIED_BY_LEGAL = "LV";//VerifiedByLegal
    }

    public static final String JOBDETAIL_COMPLETE = "Job_complete";
    public static final String JOBDETAIL_SCAN_VIN = "Scan_vin";
    public static final String JOBDETAIL_CAMERA = "Camera";

    public static final String DISPATCH_REQUEST_NUMBER = "section_number";
    public static final String DISPATCH_CREATED_DATE = "dispatch_created_date";
    public static final String BUNDLE_ALERT_RESULT = "alertResult";
    public static final String BUNDLE_POLYLINES = "PolyLines";
    public static final String ISIMAGEAVAILABLE = "isImageAvailable";
    public static final String IS_IT_FROM_HISTORY = "isItFromHistory";
    public static final String IS_FROM_SEARCH = "isFromSearch";
    public static final String IS_EDITABLE = "isEditable";
    public static final String IS_IT_FROM_EXTENUATION_CIRCUMSTANCES = "isItFromExtenCircumstances";
    public static final String NOTES = "notes";
    public static final String SERVER_DATA_NOTES = "notes";
    public static final String SERVER_DATA = "serverData";
    public static final String CURRENT_TIME = "currentTime";
    public static final String DISPATCH_ASSIGNED_ID = "dispatchAssignId";
    public static final String DISPATCH_ASSIGNED_NAME = "dispatchAssignName";
    public static final String DRIVER_ASSIGNED_BY_ID = "driverAssignedById";
    public static final String DRIVER_ASSIGNED_BY_NAME = "driverAssignedByName";
    public static final String JOB_JSON = "jobJson";

    public static final int DISPATCHER_REPORT_GOA = 1;
    public static final int DISPATCHER_REPORT_GOA_ASM = 2;
    public static final int DISPATCHER_REPORT_GOA_VRM = 3;
    public static final int DISPATCHER_SERVICE_UN_SUCCESSFUL = 4;
    public static final int DISPATCHER_CANCEL_SERVICE = 5;
    public static final int DISPATCHER_CANCEL_SERVICE_PROVIDER = 6;
    public static final int DISPATCHER_CANCEL_SERVICE_CUSTOMER = 7;
    public static final int DISPATCHER_REQUEST_JOB = 8;
    public static final int DISPATCHER_DECLINE_JOB = 9;


    public static final int DISPACTHER_UN_SUCCESSFUL_REASON = 1;
    public static final int DISPACTHER_UN_SUCCESSFUL_SERVICE_NEEDED = 2;
    public static final int DISPACTHER_UN_SUCCESSFUL_EQUIPMENT_NEEDED = 3;


    public static final String EDIT_TOW_DESTINATION = "TowDestination";
    public static final String EDIT_DISABLEMENT_LOCATION = "DisablementLocation";
    public static final String IS_IT_FROM_DISABLEMENT = "isItFromDisablement";
    public static final String IS_IT_FROM_DISABLEMENT_DIRECTION = "isItFromDisablementDir";
    public static final String IS_IT_FROM_DISABLEMENT_SIGN = "isItFromDisablementSign";
    public static final String IS_ADDRESS = "isAddress";
    public static final String TOW_JOB_DETAIL = "isTowJobDetail";
    public static final String JOB_DETAIL = "jobDetail";
    public static final String USERNAME = "username";
    public static final String CURRENT_SERVICE = "currentservice";
    public static final String UPADTED_SERVICE = "updatedservice";
    public static final String IS_DRIVER = "isdriver";
    public static final String IS_TOW = "istow";
    public static final String IMAGE_URL = "imageurl";
    public static final String VEHICLE_REPORT_ENTITIES = "vehicleReportEntities";
    public static final String VEHICLE_REPORT_DOC_INDEX = "docIndex";
    public static final String IS_OKGOTIT = "isonduty";
    public static final String IS_ONDUTYCHECKED = "isondutychecked";
    public static final String DRIVERASSUIGNTOID = "driverassigntoid";
    public static final String CURRENT_LATITUDE = "currentlatitue";
    public static final String CURRENT_LONGITUDE = "currentlongitude";
    public static final String UPDATE_ETA = "updateEta";
    public static final String OLD_UPDATE_ETA = "oldUpdatedEta";
    public static final String USER_EMAIL = "userEmail";
    public static final String JOB_STATUS = "jobStatus";

    public static final String APPTENTIVE_JOBS = "Jobs";
    public static final String APPTENTIVE_ROADBLOCK = "Roadblock";

    public static final String[] DISPATCHER_HEADERS = new String[]{"New job offers","Needs attention","On track"};

    public static final String[] COMPANY_CONTACTS_HEADERS = new String[]{"Facility","Office Team", "Drivers"};

    public static final long RETRY_LIMIT_MILLI_SEC = 5000;

    public static final String SHOW_UPDATE_ETA_BAR = "isShowUpdateETA";

    public static final String DISPATCHER_NUMBER = "dispatcherNumber";

    public static final String USER_PROFILE = "userProfile";


    public static final String ANALYTICS_WELCOME_SIGN_IN = "WelcomeSignInFragment";
    public static final String ANALYTICS_CREATE_ACCOUNT = "CreateAccountFragment";
    public static final String ANALYTICS_LEGAL = "LegalFragment";
    public static final String ANALYTICS_PERMISSION_REQUEST = "PermissionFragment";
    public static final String ANALYTICS_SIGN_IN = "PasswordFragment";
    public static final String ANALYTICS_EQUIPMENT_SELCTION = "EquipmentSelectionFragment";
    public static final String ANALYTICS_ACTIVE_JOBS = "ActiveJobsFragment";
    public static final String ANALYTICS_MODIFY_STATUS = "JobStatusFragment";
    public static final String ANALYTICS_SCAN_VIN = "ScanVinBarCodeFragment";
    public static final String ANALYTICS_ADD_ATTACHMENTS = "VehicleReportFragment";
    public static final String ANALYTICS_TAG_ATTACHMENTS = "VehicleReportTaggingFragment";
    public static final String ANALYTICS_JOB_NOTES = "NotesFragment";
    public static final String ANALYTICS_ASSIGN_DRIVER = "AssignDriverFragment";
    public static final String ANALYTICS_SERVICE_UNSUCCESSFUL = "ServiceUnsuccessfulFragment";
    public static final String ANALYTICS_EDIT_SERVICE = "EditServiceFragment";
    public static final String ANALYTICS_JOB_HISTORY = "HistoryFragment";
    public static final String ANALYTICS_ALERTS = "AlertFragment";
    public static final String ANALYTICS_ACCOUNT = "AccountFragment";
    public static final String ANALYTICS_EDIT_PROFILE = "EditProfileFragment";
    public static final String ANALYTICS_UPDATE_PASSWORD = "UpdatedPasswordFragment";
    public static final String ANALYTICS_COMPANY = "MyCompanyFragment";
    public static final String ANALYTICS_HELP = "ZendeskHelpFragment";

    public static final String SERVICE_TYPE_TOW= "TOW";
    public static final String SERVICE_TYPE_VRTOW= "VRTOW";
    public static final String SERVICE_TYPE_HTOW= "HTOW";
    public static final String SERVICE_TYPE_LTOW= "LTOW";
    public static final String SERVICE_TYPE_ASTOW= "ASLTOW";
    public static final String SERVICE_TYPE_MTOW= "MTOW";
    public static final String SERVICE_TYPE_ROADSIDE= "ROADSIDE";
    public static final String SERVICE_TYPE_WINCH= "WINCH";
    public static final String SERVICE_TYPE_MWNH= "MWNH";
    public static final String SERVICE_TYPE_HFLD= "HFLD";
    public static final String SERVICE_TYPE_LFLD= "LFLD";
    public static final String SERVICE_TYPE_LFLT= "LFLT";
    public static final String SERVICE_TYPE_LWNH= "LWNH";
    public static final String SERVICE_TYPE_LKTSMITH= "LKTSMITH";
    public static final String SERVICE_TYPE_MCPK= "MCPK";
    public static final String SERVICE_TYPE_HJMS= "HJMS";
    public static final String SERVICE_TYPE_LJMS= "LJMS";
    public static final String SERVICE_TYPE_MFLD= "MFLD";
    public static final String SERVICE_TYPE_MFLT= "MFLT";
    public static final String SERVICE_TYPE_MJMS= "MJMS";
    public static final String SERVICE_TYPE_HFLT= "HFLT";
    public static final String SERVICE_TYPE_VSTG= "VSTG";
    public static final String SERVICE_TYPE_LKT= "LKT";
    public static final String SERVICE_TYPE_HWNH= "HWNH";

    public static final String ALERT_ACTION = "alert_receive";
    public static final String CHAT_ACTION = "chat_receive";

    public static final String[] JOB_STATUS_CANCEL_JOB_DISPATCHER_OPTIONS = new String[]{"PARTNER_CANCEL_REQUEST","PARTNER_CANCEL_REQUEST","PARTNER_CANCEL_REQUEST_CUSTOMER","PARTNER_CANCEL_REQUEST","PARTNER_CANCEL_REQUEST","PARTNER_CANCEL_REQUEST","PARTNER_CANCEL_REQUEST_CUSTOMER","PARTNER_CANCEL_REQUEST"};

    public static final String[] JOB_STATUS_CANCEL_JOB_CUSTOMER_OPTIONS = new String[]{"customerFoundSolution","selfCorrected","notArrivedAtETA","reasonNotListed"};


    public static final String API_JOB_ACCEPT = "jobAcceptApi";
    public static final String API_JOB_DECLINE = "jobDeclineApi";
    public static final String API_JOB_STATUS_UPDATE = "jobStatusUpdateApi";
    public static final String API_JOB_STATUS_UNDO_UPDATE = "jobStatusUndoApi";
    public static final String API_JOB_ASSIGNMENT = "jobAssignDriverApi";
    public static final String API_JOB_CANCEL = "jobCancelApi";
    public static final String API_JOB_ETA_UPDATE = "jobEtaChangeApi";
    public static final String API_JOB_DL_UPDATE = "jobDisablementLocationChangeApi";
    public static final String API_JOB_TL_UPDATE = "jobTowLocationChangeApi";
    public static final String API_JOB_STATUS_GOA_UPDATE = "jobGoaApi";
    public static final String API_DRIVER_STATUS_UPDATE = "driverStatusUpdateApi";

    public static final String USER_STATUS_INACTIVE = "inactive";
    public static final String USER_STATUS_ACTIVE = "active";
    public static final String USER_STATUS_BLOCKED = "blocked";
    public static final String USER_STATUS_VERIFIED= "verified";
    public static final String USER_STATUS_UNVERIFIED = "unverified";
    public static final String USER_STATUS_DELETED = "deleted";

    public static final String TIME_ZONE = "timeZone";

    public static final String JOB_DISPATCH_SOURCE_ASM_VRM = "am";
    public static final String JOB_DISPATCH_SOURCE_ONEROAD = "or";
    public static final String JOB_SERVICE_TYPE_ASM = "AS";

    public static final String JOB_TYPE_VRM = "VRM";
    public static final String JOB_TYPE_ASM = "ASM";
    public static final String JOB_TYPE_VRM_ASP = "VRM-A-SP";
    public static final String JOB_TYPE_VRM_A_AGERO = "VRM-A-AGERO";

    public static final boolean IS_GEOFENCE_ENABLED = true;

    public static final String CHAT_USERS_LIST = "chatUsersList";
    public static final String CHAT_THREAD_TYPE = "chatThreadType";
    public static final String CHAT_THREAD_NAME = "chatThreadName";

    public static final String FORGOT_PWD_COUNTER = "forgotPwdCounter";
    public static final boolean IS_CHAT_ENABLED = true;
    public static final boolean IS_FINGERPRINT_ENABLED = true;

    public static final String NOTIFICATION_TYPE = "notificationType";
    public static final String NOTIFICATION_TYPE_CHAT = "CHAT";
    public static final String NOTIFICATION_ENTITY_ID = "entityId";

    public static final String CHAT_BADGE_COUNT = "ChatUnReadCount";
    public static final String CHAT_MESSAGE_COUNT = "ChatUnReadMessageCount";


}
