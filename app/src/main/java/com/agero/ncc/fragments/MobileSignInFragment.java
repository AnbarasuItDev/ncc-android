package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.utils.UserError;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MobileSignInFragment extends BaseFragment {

    WelcomeActivity mWelcomeActivity;
    UserError mUserError;
    @BindView(R.id.text_label_welcome)
    TextView mTextLabelWelcome;
    @BindView(R.id.edit_email)
    EditText mEditEmail;
    @BindView(R.id.edit_password)
    EditText mEditPassword;
    @BindView(R.id.button_signin)
    Button mButtonSignin;
    ForgotPasswordDialogFragment forgotPasswordDialogFragment;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_mobile_signin, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mWelcomeActivity = (WelcomeActivity) getActivity();
//        mWelcomeActivity.hideBottomBar();
        Toolbar toolbar = (Toolbar) mWelcomeActivity.findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_text);
        textView.setText("");
        mUserError = new UserError();
        mButtonSignin.setEnabled(false);
        mEditEmail.addTextChangedListener(textwatcher);
        mEditPassword.addTextChangedListener(textwatcher);
//        forgotPasswordDialogFragment = ForgotPasswordDialogFragment.newInstance();
        return superView;
    }


    private void enableSignButton() {
        if (isValidEmail(mEditEmail.getText().toString().trim()) && !mEditPassword.getText().toString().isEmpty()) {
            mButtonSignin.setTextColor(getResources().getColor(R.color.ncc_white));
            mButtonSignin.setBackgroundResource(R.color.ncc_blue);
            mButtonSignin.setEnabled(true);
        } else {
            mButtonSignin.setTextColor(getResources().getColor(R.color.ncc_button_label_color));
            mButtonSignin.setBackgroundResource(R.color.ncc_button_color);
            mButtonSignin.setEnabled(false);
        }
    }

    //    boolean checkPhoneNumber(String number) {
//        if (number.length() != 10) {
//            return false;
//        }
//        return Patterns.PHONE.matcher(number).matches();
//    }

    @OnClick({R.id.button_signin, R.id.text_forgot_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_signin:
                mWelcomeActivity.pushFragment(PermissionFragment.newInstance());
                break;
            case R.id.text_forgot_password:
//                forgotPasswordDialogFragment.show(getActivity().getFragmentManager(), "");
                break;
        }
    }

    TextWatcher textwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            enableSignButton();
        }
    };


//    public static boolean isValidPassword(String password) {
//
//        Pattern pattern;
//        Matcher matcher;
//        String PASSWORD_PATTERN = "^(?=.*?[A-Z])(?=.*?[0-9]).{8,}$";
//        pattern = Pattern.compile(PASSWORD_PATTERN);
//        matcher = pattern.matcher(password);
//        return matcher.matches();
//
//    }

}
