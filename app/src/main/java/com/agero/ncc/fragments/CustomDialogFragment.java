package com.agero.ncc.fragments;


import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.driver.fragments.ActiveJobsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomDialogFragment extends DialogFragment {

    HomeActivity mHomeActivity;

    public CustomDialogFragment() {

    }

    public static CustomDialogFragment newInstance() {
        CustomDialogFragment fragment = new CustomDialogFragment();
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.customdialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_custom_fragment, container);
        ButterKnife.bind(this, v);
        mHomeActivity = (HomeActivity)getActivity();
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    public void run() {
                        if(isAdded()) {
                            try {
                                dismiss();
                                mHomeActivity.popBackStackImmediate();
                            }catch (Exception e){

                            }
                            if (mHomeActivity.isLoggedInDriver()) {
                                mHomeActivity.push(ActiveJobsFragment.newInstance());
                            } else {
                                mHomeActivity.push(com.agero.ncc.dispatcher.fragments.ActiveJobsFragment.newInstance());
                            }

                        }
                    }
                });
            }
        }).start();

        return v;
    }
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog!=null){
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

}
