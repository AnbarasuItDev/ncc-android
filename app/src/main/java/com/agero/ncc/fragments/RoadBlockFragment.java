package com.agero.ncc.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.agero.ncc.R;
import com.agero.ncc.utils.NccConstants;
import com.apptentive.android.sdk.Apptentive;

import butterknife.ButterKnife;


public class RoadBlockFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_roadblock, fragment_content, true);
        ButterKnife.bind(this, view);
        Apptentive.engage(getActivity(), NccConstants.APPTENTIVE_ROADBLOCK);
        return superView;
    }
}
