package com.agero.ncc.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

import java.util.Date

@Entity(tableName = "alert")
class Alert {

    @PrimaryKey(autoGenerate = true)
    var alertId: Long = 0

    @ColumnInfo(name = "getDisplayStatusText")
    var status: String? = null

    @ColumnInfo(name = "alert_title")
    var alertTitle: String? = null

    @ColumnInfo(name = "alert_description")
    var alertDescription: String? = null

    @ColumnInfo(name = "read")
    var read: Boolean? = null

    @ColumnInfo(name = "updated_time")
    var updatedTime: Date? = null
}
